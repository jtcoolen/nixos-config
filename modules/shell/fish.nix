{ config, options, pkgs, inputs, lib, ... }:
with lib;
let cfg = config.modules.shell.fish;
in
{
  options.modules.shell.fish = {
    enable = mkOption {
      default = false;
      type = with types; bool;
    };
  };

  config = mkIf cfg.enable {
  programs.fish.enable = true;
    home-manager.users.jco = { pkgs, ... }: {
      programs.fish = {
        enable = true;
        promptInit = "any-nix-shell fish --info-right | source";
        shellInit = "source /home/jco/.opam/opam-init/init.fish > /dev/null 2> /dev/null; or true";
        shellAbbrs = {
          tpaste = "curl -F 'tpaste=<-' https://tpaste.us/";
          bpaste = "curl -X POST https://bpa.st/curl -F 'raw=<-'";
          vim = "nvim";
        };
      };
    };
  };
}

