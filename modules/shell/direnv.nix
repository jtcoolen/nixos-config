{ config, options, pkgs, inputs, lib, ... }:
with lib;
let cfg = config.modules.shell.direnv;
in
{
  options.modules.shell.direnv = {
    enable = mkOption {
      default = false;
      type = with types; bool;
    };

  };

  config = mkIf cfg.enable {
    home-manager.users.jco = { pkgs, ... }: {
      programs.direnv.enable = true;
      programs.direnv.enableFishIntegration = true;
    };
  };
}
