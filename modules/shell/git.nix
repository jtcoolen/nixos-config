{ config, options, pkgs, inputs, lib, ... }:
with lib;
let cfg = config.modules.shell.git;
in
{
  options.modules.shell.git = {
    enable = mkOption {
      default = false;
      type = with types; bool;
    };

  };

  config = mkIf cfg.enable {
    home-manager.users.jco = { pkgs, ... }: {
      programs.git = {
        enable = true;
        package = pkgs.gitAndTools.gitFull;
        userName = "Julien Coolen";
        userEmail = "jtcoolen@pm.me";
        signing = {
          key = "4C6856EEDFDA20FB77E8916919642151C218F6F5";
          signByDefault = true;
        };
        delta = {
          enable = true;
          options = { decorations = { file-decoration-style = "none"; }; features = "decorations side-by-side"; syntax-theme = "gruvbox-light"; line-numbers = true; };
        };
      };
    };
  };
}
