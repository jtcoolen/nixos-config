{ config, options, lib, pkgs, ... }:

with lib;
let cfg = config.modules.dev.smartcards;
in
{
  options.modules.dev.smartcards = {
    enable = mkOption {
      default = false;
      type = with types; bool;
    };
  };

  config = mkIf cfg.enable {
    services.pcscd.enable = true;
    environment.systemPackages = (with pkgs; [
      pcsctools
    ]);
  };
}
