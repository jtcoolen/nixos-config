{ config, options, lib, pkgs, ... }:

with lib;
let cfg = config.modules.dev.ocaml;
in
{
  options.modules.dev.ocaml = {
    enable = mkOption {
      default = false;
      type = with types; bool;
    };
  };

  config = mkIf cfg.enable {
    environment.systemPackages = (with pkgs; [
      ocaml
      dune_2
      opam
      # dynamic dependencies for libraries must be installed with nix-shell
      # eg. 'nix-shell -p m4 pkgconfig gmp openssl'...
    ]) ++ (with pkgs.ocamlPackages; [
      utop
      lsp
      ocaml-lsp
    ]);
  };
}
