{ config, options, lib, pkgs, inputs, ... }:
# Note: run once
# rustup install stable
# rustup default stable
# in order to install and set the default toolchain
with lib;
let
  cfg = config.modules.dev.rust;
in
{
  options.modules.dev.rust = {
    enable = mkOption {
      default = false;
      type = with types; bool;
    };
  };

  config = mkIf cfg.enable {
    nixpkgs.overlays = [ inputs.rust-overlay.overlay ];
    environment.systemPackages = (with pkgs; [
      rust-bin.stable.latest.default
    ]);
  };
}
