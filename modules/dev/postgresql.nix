{ config, options, lib, pkgs, ... }:

with lib;
let cfg = config.modules.dev.postgresql;
in
{
  options.modules.dev.postgresql = {
    enable = mkOption {
      default = false;
      type = with types; bool;
    };
  };

  config = mkIf cfg.enable {
    nixpkgs.config.permittedInsecurePackages = [
      "openssl-1.0.2u"
    ];
    services.postgresql = {
      enable = true;
    };
  };
}
