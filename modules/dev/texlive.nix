{ config, options, lib, pkgs, ... }:

with lib;
let cfg = config.modules.dev.texlive;
in
{
  options.modules.dev.texlive = {
    enable = mkOption {
      default = false;
      type = with types; bool;
    };
  };

  config = mkIf cfg.enable {
    environment.systemPackages = (with pkgs; [
      texlive.combined.scheme-basic
      pygmentex
      python37Packages.pygments
    ]);
  };
}
