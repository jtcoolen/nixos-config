{ config, options, lib, pkgs, ... }:

with lib;
let cfg = config.modules.dev.networking;
in
{
  options.modules.dev.networking = {
    enable = mkOption {
      default = false;
      type = with types; bool;
    };
  };

  config = mkIf cfg.enable {
    programs.wireshark.enable = true;
    environment.systemPackages = (with pkgs; [
      tcpdump
      nmap
      nmap-graphical
    ]);
  };
}
