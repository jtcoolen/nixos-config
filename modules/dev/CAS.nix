{ config, options, lib, pkgs, ... }:

with lib;
let cfg = config.modules.dev.CAS; # Computer Algebra Systems (CAS)
in
{
  options.modules.dev.CAS = {
    enable = mkOption {
      default = false;
      type = with types; bool;
    };
  };

  config = mkIf cfg.enable {
    environment.systemPackages = (with pkgs; [
      # PARI/GP
      pari
      pari-galdata
      pari-seadata-small
      gp2c
      python37Packages.cypari2
      python38Packages.cypari2
      # GAP
      gap
      gap-full
    ]);
  };
}
