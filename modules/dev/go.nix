{ config, options, lib, pkgs, ... }:

with lib;
let cfg = config.modules.dev.go;
in
{
  options.modules.dev.go = {
    enable = mkOption {
      default = false;
      type = with types; bool;
    };
  };

  config = mkIf cfg.enable {
    environment.systemPackages = (with pkgs; [
      go
    ]);
  };
}
