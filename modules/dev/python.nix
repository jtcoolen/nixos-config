{ config, options, lib, pkgs, ... }:

with lib;
let cfg = config.modules.dev.python;
in
{
  options.modules.dev.python = {
    enable = mkOption {
      default = false;
      type = with types; bool;
    };
  };

  config = mkIf cfg.enable {
    environment.systemPackages = (with pkgs; [
      python38
      qt5.qtwayland
      python38Packages.pandas
      python38Packages.jupyterlab
      python38Packages.ipython
      python38Packages.spyder-kernels
      python38Packages.spyder
      python38Packages.scipy
      python38Packages.matplotlib
      python38Packages.numpy
      python38Packages.flake8
      python38Packages.pylint
      python38Packages.autopep8
    ]);
  };
}
