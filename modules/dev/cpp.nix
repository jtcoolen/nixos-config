{ config, options, lib, pkgs, ... }:

with lib;
let cfg = config.modules.dev.cpp;
in
{
  options.modules.dev.cpp = {
    enable = mkOption {
      default = false;
      type = with types; bool;
    };
  };

  config = mkIf cfg.enable {
    environment.systemPackages = (with pkgs; [
      gnumake
      cmake
      clang
      gcc
      bear
      gdb
      llvmPackages.libcxx
      valgrind
      jetbrains.clion
    ]);
  };
}
