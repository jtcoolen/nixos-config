{ options, config, lib, pkgs, ... }:

with lib;
let cfg = config.modules.dev.qemu;
in
{
  options.modules.dev.qemu = {
    enable = mkOption {
      default = false;
      type = with types; bool;
    };
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      qemu
      kvm
      qemu_kvm
      qemu-utils
    ];
  };
}
