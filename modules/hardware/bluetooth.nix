{ options, config, pkgs, lib, ... }:
with lib;
let cfg = config.modules.hardware.bluetooth;
in
{
  options.modules.hardware.bluetooth = {
    enable = mkOption {
      default = false;
      type = with types; bool;
    };
  };

  config = mkIf cfg.enable
    {
      hardware.bluetooth.enable = true;
      services.blueman.enable = true;
      services.pipewire = {
        media-session.config = {
          media-session = {
            "session.modules".default = [ "metadata" "flatpak" "portal" "v4l2" "policy-node" "suspend-node" "bluez5" ];
          };
          bluez-monitor.properties = {
            "bluez5.sbc-xq-support" = true;
            "bluez5.headset-roles" = [ "hfp_ag" ];
          };
          bluez-monitor.rules = [
            {
              matches = [{ "device.name" = "~bluez_card.*"; }];
              actions = {
                "update-props" = {
                  "bluez5.reconnect-profiles" = [ "hfp_hf" "hsp_hs" "a2dp_sink" ];
                  "bluez5.msbc-support" = true;
                };
              };
            }
            {
              matches = [
                { "node.name" = "~bluez_input.*"; }
                { "node.name" = "~bluez_output.*"; }
              ];
              actions = {
                "update-props" = {
                  "node.pause-on-idle" = false;
                };
              };
            }
          ];
        };
      };
    };
}
