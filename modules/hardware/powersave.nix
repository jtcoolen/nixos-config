{ options, config, pkgs, lib, ... }:
with lib;
let cfg = config.modules.hardware.powersave;
in
{
  options.modules.hardware.powersave = {
    enable = mkOption {
      default = false;
      type = with types; bool;
    };

  };

  config = mkIf cfg.enable {

    #powerManagement = {
    #  enable = true;
    #  powertop.enable = true;
    #};

    services.tlp = {
      enable = true;
      settings = {
        USB_AUTOSUSPEND = 0;
      };
    };

  };
}
