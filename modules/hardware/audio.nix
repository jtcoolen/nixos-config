{ options, config, pkgs, lib, ... }:
with lib;
let cfg = config.modules.hardware.audio;
in
{
  options.modules.hardware.audio = {
    enable = mkOption {
      default = false;
      type = with types; bool;
    };

  };

  config = mkIf cfg.enable {

    #nixpkgs.overlays = [
    #     (self: super:
    #       let
    #         nixpkgs-pipewire = builtins.fetchTarball {
    #           name = "pipewire-pin";
    #           url = "https://github.com/nixos/nixpkgs/archive/890b13b38c1f91bbfe0474b0473679d29873c2cc.tar.gz";
    #           sha256 = "sha256:1fxxygbmqx6r5asq225675pyy65957309za7ivmzq29v3d7jd1s3";
    #         };
    #       in
    #       { pipewire = (import nixpkgs-pipewire { system = "x86_64-linux"; }).pipewire; })
    #   ];


    nixpkgs.config.pulseaudio = true;
    #hardware.pulseaudio.enable = true; # we're trying pipewire
    hardware.pulseaudio.enable = pkgs.lib.mkForce false;
    security.rtkit.enable = true;

    environment.systemPackages = with pkgs; [
      #wireplumber
      pavucontrol
      qjackctl
    ];

    services.pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true; # ?
      pulse.enable = true;
      package = pkgs.pipewire;
      jack.enable = true;
    };



  };
}
