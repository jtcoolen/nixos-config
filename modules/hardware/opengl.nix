{ options, config, pkgs, lib, ... }:
with lib;
let cfg = config.modules.hardware.opengl;
in
{
  options.modules.hardware.opengl = {
    enable = mkOption {
      default = false;
      type = with types; bool;
    };

  };

  config = mkIf cfg.enable {

    hardware = {
      opengl = {
        enable = true;
        package = pkgs.mesa_drivers;
        driSupport32Bit = true;
        extraPackages = (with pkgs; [
          intel-media-driver
          vaapiIntel
          vaapiVdpau
          libvdpau-va-gl
        ]);

      };
    };
  };
}
