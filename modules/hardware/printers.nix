{ options, config, pkgs, lib, ... }:
with lib;
let cfg = config.modules.hardware.printers;
in
{
  options.modules.hardware.printers = {
    enable = mkOption {
      default = false;
      type = with types; bool;
    };

  };

  config = mkIf cfg.enable {
    # Enable CUPS to print documents.
    services.printing.enable = true;
    services.avahi.enable = true; # Discover hosts on local network
    # Important to resolve .local domains of printers, otherwise you get an error
    # like  "Impossible to connect to XXX.local: Name or service not known"
    #services.avahi.nssmdns = true;
    services.printing.drivers = [ pkgs.brgenml1lpr pkgs.brgenml1cupswrapper pkgs.brlaser ];
    # Scanner support.
    hardware = {
      sane = {
        enable = true;
        brscan4 = {
          enable = true;
          netDevices = {
            home = {
              model = "MFC-7360N";
              ip = "192.168.001.100";
            };
          };
        };
      };
    };
  };
}
