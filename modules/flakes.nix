{ config, pkgs, inputs, ... }:

{
  # Install the flakes edition
  nix.package = pkgs.nixFlakes;
  nix.extraOptions = ''
    experimental-features = nix-command flakes ca-references recursive-nix
  '';
}
