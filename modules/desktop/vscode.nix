{ pkgs, config, inputs, options, lib, ... }:
with lib;
let cfg = config.modules.desktop.vscode;
in
{
  options.modules.desktop.vscode = {
    enable = mkOption {
      default = false;
      type = with types; bool;
    };
  };

  config = mkIf cfg.enable {
    home-manager.users.jco = { pkgs, ... }: {
      programs.vscode = {
        enable = true;
        package = let pname = "vscode"; in
          (pkgs.runCommandNoCC "${pname}"
            { inherit pname; buildInputs = with pkgs; [ makeWrapper ]; }
            ''
              makeWrapper ${pkgs.vscode}/bin/code $out/bin/code \
                --add-flags "--enable-features=UseOzonePlatform" \
                --add-flags "--ozone-platform=wayland"
              ln -sf ${pkgs.vscode}/share $out/share
            ''
          );
        extensions = with pkgs.vscode-extensions;
          [
            vscodevim.vim
            eamodio.gitlens
            jnoortheen.nix-ide
            brettm12345.nixfmt-vscode
            ms-vscode.cpptools
            ocamllabs.ocaml-platform
            ms-python.python
            golang.Go
            haskell.haskell
            james-yu.latex-workshop
            ms-azuretools.vscode-docker
            ms-vsliveshare.vsliveshare
            tomoki1207.pdf
            timonwong.shellcheck
          ] ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [
            {
              name = "lean";
              publisher = "jroesch";
              version = "0.16.28";
              sha256 = "sha256-5cNWorCsABE2bNXYi+/7SZYXLQKWrAC1Z+34ivTV/Mw=";
            }
            {
              name = "vscoq";
              publisher = "maximedenes";
              version = "0.3.4";
              sha256 = "sha256-fBgBD1IUipl/GgN5+htZ1WZLX+smsPFSbBgI+uIsh4I=";
            }
            {
              name = "rust";
              publisher = "rust-lang";
              version = "0.7.8";
              sha256 = "039ns854v1k4jb9xqknrjkj8lf62nfcpfn0716ancmjc4f0xlzb3";
            }
          ];
        userSettings = {
          "workbench.colorTheme" = "Visual Studio Light";
          "editor.fontFamily" = "monospace";
          "editor.fontSize" = 16;
          "window.menuBarVisibility" = "toggle";
          "editor.minimap.enabled" = false;
          "workbench.activityBar.visible" = false;
          "gitlens.mode.active" = "zen";
          "terminal.integrated.enableBell" = true;
          "terminal.integrated.fontSize" = 15;
          "editor.lineNumbers" = "interval";
          "editor.renderIndentGuides" = false;
          "terminal.integrated.shell.linux" = "/run/current-system/sw/bin/fish";
          "editor.tabSize" = 2;
          "editor.smoothScrolling" = true;
          "[nix]" = { "editor.defaultFormatter" = "jnoortheen.nix-ide"; };
        };
      };
    };
  };
}
