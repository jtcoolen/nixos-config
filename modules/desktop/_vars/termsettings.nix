{ pkgs, ... }:

{
  fonts = rec {
    # TODO: nothing ensures the font is available in
    # HM modules that don't use `fonts.default.package`

    default = ibm_plex;

    ibm_plex = {
      name = "IBM Plex Mono";
      size = 11;
      package = pkgs.ibm-plex;
    };
  };

  colors = rec {
    default = modus;

    modus = {
      cursorColor = "#FFFFFF";

      foreground = "#000000";
      background = "#ffffff";

      dimForeground = "#282828";
      brightForeground = "#000000";

      black = "#000000";
      red = "#a60000";
      green = "#005e00";
      yellow = "#813e00";
      blue = "#0030a6";
      purple = "#721045";
      cyan = "#00538b";
      white = "#808080";

      brightBlack = "#eeeeee";
      brightRed = "#972500";
      brightGreen = "#315b00";
      brightYellow = "#70480f";
      brightBlue = "#223fbf";
      brightPurple = "#8f0075";
      brightCyan = "#30517f";
      brightWhite = "#eeeeee";

      dimBlack = "#555555";
      dimRed = "#4d0006";
      dimGreen = "#003000";
      dimYellow = "#3a2a00";
      dimBlue = "#001170";
      dimPurple = "#381050";
      dimCyan = "#003434";
      dimWhite = "#eeeeee";
    };
  };
}
