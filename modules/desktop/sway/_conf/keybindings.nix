{ pkgs }:
let
  modifier = "Mod4";
  terminal = "alacritty";
  browser = "firefox";
  editor = "${pkgs.vscodium}/bin/codium";

  swaylockcmd = "${pkgs.swaylock}/bin/swaylock -f -c '#000000'";
  idlecmd = ''
    ${pkgs.swayidle}/bin/swayidle -w \
        before-sleep \"${swaylockcmd}\" \
        lock \"${swaylockcmd}\" \
        timeout 500 \"${swaylockcmd}\" \
        timeout 1000 \"${pkgs.systemd}/bin/systemctl suspend\"'';
in
{
  modifier = modifier;
  keybindings = {
    "${modifier}+d" =
      "exec bemenu-run --fn 'IBM Plex Sans Bold 10'";

    # exit sway (logs you out of your session)
    "${modifier}+Shift+e" = "exec ${pkgs.sway}/bin/swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -b 'Yes, exit sway' 'swaymsg exit'";

    "${modifier}+Return" = "exec ${terminal}";
    "${modifier}+Shift+f" = "exec ${browser}";
    "${modifier}+Shift+q" = "kill";
    "${modifier}+Ctrl+l" = "${swaylockcmd}";
    "XF86ScreenSaver" = "${swaylockcmd}";

    "${modifier}+Left" = "focus left";
    "${modifier}+Down" = "focus down";
    "${modifier}+Up" = "focus up";
    "${modifier}+Right" = "focus right";

    "${modifier}+Shift+Left" = "move left";
    "${modifier}+Shift+Down" = "move down";
    "${modifier}+Shift+Up" = "move up";
    "${modifier}+Shift+Right" = "move right";

    "${modifier}+b" = "splith";
    "${modifier}+v" = "splitv";
    "${modifier}+f" = "fullscreen toggle";
    "${modifier}+a" = "focus parent";

    "${modifier}+s" = "layout stacking";
    "${modifier}+w" = "layout tabbed";
    "${modifier}+e" = "layout toggle split";

    "${modifier}+Shift+space" = "floating toggle";
    "${modifier}+Shift+Alt+space" = "sticky toggle";
    "${modifier}+space" = "focus mode_toggle";

    # For AZERTY layout
    "${modifier}+ampersand" = "workspace number 1";
    "${modifier}+eacute" = "workspace number 2";
    "${modifier}+quotedbl" = "workspace number 3";
    "${modifier}+apostrophe" = "workspace number 4";
    "${modifier}+parenleft" = "workspace number 5";
    "${modifier}+minus" = "workspace number 6";
    "${modifier}+egrave" = "workspace number 7";
    "${modifier}+underscore" = "workspace number 8";
    "${modifier}+ccedilla" = "workspace number 9";
    "${modifier}+agrave" = "workspace number 10";

    "${modifier}+Shift+ampersand" = "move container to workspace number 1";
    "${modifier}+Shift+eacute" = "move container to workspace number 2";
    "${modifier}+Shift+quotedbl" = "move container to workspace number 3";
    "${modifier}+Shift+apostrophe" = "move container to workspace number 4";
    "${modifier}+Shift+parenleft" = "move container to workspace number 5";
    "${modifier}+Shift+minus" = "move container to workspace number 6";
    "${modifier}+Shift+egrave" = "move container to workspace number 7";
    "${modifier}+Shift+underscore" = "move container to workspace number 8";
    "${modifier}+Shift+ccedilla" = "move container to workspace number 9";
    "${modifier}+Shift+agrave" = "move container to workspace number 10";

    "${modifier}+1" = "workspace number 1";
    "${modifier}+2" = "workspace number 2";
    "${modifier}+3" = "workspace number 3";
    "${modifier}+4" = "workspace number 4";
    "${modifier}+5" = "workspace number 5";
    "${modifier}+6" = "workspace number 6";
    "${modifier}+7" = "workspace number 7";
    "${modifier}+8" = "workspace number 8";
    "${modifier}+9" = "workspace number 9";
    "${modifier}+0" = "workspace number 10";

    "${modifier}+Shift+1" = "move container to workspace number 1";
    "${modifier}+Shift+2" = "move container to workspace number 2";
    "${modifier}+Shift+3" = "move container to workspace number 3";
    "${modifier}+Shift+4" = "move container to workspace number 4";
    "${modifier}+Shift+5" = "move container to workspace number 5";
    "${modifier}+Shift+6" = "move container to workspace number 6";
    "${modifier}+Shift+7" = "move container to workspace number 7";
    "${modifier}+Shift+8" = "move container to workspace number 8";
    "${modifier}+Shift+9" = "move container to workspace number 9";
    "${modifier}+Shift+0" = "move container to workspace number 10";

    #"${modifier}+Shift+minus" = "move scratchpad";
    #"${modifier}+minus" = "scratchpad show";

    "${modifier}+r" = "resize";

    "Print" =
      ''exec grim -g \"$(slurp)" Images/$(date +'%Y-%m-%d-%H%M%S.png')'';

    "XF86AudioRaiseVolume" = "exec ${pkgs.pulsemixer}/bin/pulsemixer --change-volume +2";
    "XF86AudioLowerVolume" = "exec ${pkgs.pulsemixer}/bin/pulsemixer --change-volume -2";
    "XF86AudioMute" = "exec --no-startup-id pactl set-sink-mute 0 toggle";
    "XF86MonBrightnessUp" = "exec light -A 2";
    "XF86MonBrightnessDown" = "exec light -U 2";
  };
}
