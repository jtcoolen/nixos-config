{ pkgs }: {
  #window.border = 1;
  window.titlebar = true;
  window.commands = [
    {
      criteria = { app_id = "mpv"; };
      command = "sticky enable";
    }
    {
      criteria = { app_id = "mpv"; };
      command = "floating enable";
    }
  ];
  floating.criteria = [
    { instance = "pinentry"; }
    { title = "Firefox - Sharing Indicator"; }
  ];
}
