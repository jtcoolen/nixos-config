{ pkgs }: {
  colors = {
    focused = {
      background = "#4d4d4d";
      border = "#4d4d4d";
      childBorder = "#4d4d4d";
      indicator = "#4d4d4d";
      text = "#ffffff";
    };
    focusedInactive = {
      background = "#222222";
      border = "#333333";
      childBorder = "#222222";
      indicator = "#222222";
      text = "#ffffff";
    };
    unfocused = {
      background = "#222222";
      border = "#333333";
      childBorder = "#222222";
      indicator = "#222222";
      text = "#ffffff";
    };
    urgent = {
      background = "#900000";
      border = "#2f343a";
      childBorder = "#900000";
      indicator = "#900000";
      text = "#ffffff";
    };
    placeholder = {
      background = "#0c0c0c";
      border = "#000000";
      childBorder = "#0c0c0c";
      indicator = "#000000";
      text = "#ffffff";
    };
  };
}
