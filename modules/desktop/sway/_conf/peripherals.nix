{ pkgs }:
let
  x230_keyboard = "1:1:AT_Translated_Set_2_keyboard";
  x230_touchpad = "2:7:SynPS/2_Synaptics_TouchPad";
  x240_touchpad = "1739:0:Synaptics_TM2749-001";
  apple_keyboard = "1452:592:Apple_Inc._Apple_Keyboard";
  hhkb =
    let
      l = builtins.map
        (input: {
          name = input;
          value = {
            xkb_layout = "us";
            xkb_variant = "altgr-intl";
          };
        })
        [
          "1278:33:PFU_Limited_HHKB-Hybrid"
          "1278:33:PFU_Limited_HHKB-Hybrid_Keyboard"
          "1278:33:HHKB-Hybrid_1_Keyboard"
          "1278:33:HHKB-Hybrid_2_Keyboard"
          "1278:33:HHKB-Hybrid_3_Keyboard"
          "1278:33:HHKB-Hybrid_4_Keyboard"
        ];
    in
    builtins.listToAttrs l;
in
{
  input = {
    "${x230_keyboard}" = {
      xkb_layout = "fr";
      xkb_model = "pc101";
    };
    "${x230_touchpad}" = {
      click_method = "clickfinger";
      tap = "enabled";
      dwt = "enabled";
      scroll_method = "two_finger";
      natural_scroll = "disabled";
      accel_profile = "adaptive";
      pointer_accel = ".5";
    };
    "${apple_keyboard}" = {
      xkb_layout = "fr";
      xkb_model = "applealu_ansi";
    };
  } // hhkb;
  output = { "*" = { background = "#eeeeee solid_color"; }; };
}
