{ pkgs }: {
  startup = [
    #    { command = "exec dbus-update-activation-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=sway"; }
    {
      always = true;
      command = "${pkgs.systemd}/bin/systemd-notify --ready || true";
    }
    {
      always = true;
      command = "${pkgs.mako}/bin/mako";
    }

    #{
    #  always = true;
    #  command = "pkill swayidle";
    #} # Disable swayidle for a bit
    #{ command = "${idlecmd}"; always = true; }     # Disable swayidle for a bit

    #{
    #  always = true;
    #  command = "systemctl --user import-environment";
    #}
    {
      always = true;
      command = "exec dbus-update-activation-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=sway";
    }
  ];
}
