{ pkgs, config, ... }:

{
  config = {
    home-manager.users.jco = { pkgs, ... }: {
      programs.waybar = {
        enable = true;
        style = pkgs.lib.readFile ./waybar.css;
        settings = [{
          layer = "top";
          position = "bottom";
          modules-left = [
            "sway/mode"
            "sway/workspaces"
          ];
          modules-center = [ ];
          modules-right =
            [
              "pulseaudio"
              "network"
              "cpu"
              "temperature"
              "memory"
              "clock"
              "battery"
            ];

          modules =
            {
              "sway/workspaces" = {
                all-outputs = true;
                disable-scroll-wraparound = true;
                enable-bar-scroll = true;
              };
              "sway/mode" = { tooltip = true; };

              #"idle_inhibitor" = { format = "{icon}"; };
              pulseaudio = {
                format = "{desc} {volume}%";
                on-click-middle = "${pkgs.pavucontrol}/bin/pavucontrol";
              };
              network = {
                format-wifi = "{ifname} ssid({essid} {signalStrength}%) up({bandwidthUpBits}) down({bandwidthDownBits})";
                format-ethernet = "{ifname} up({bandwidthUpBits}) down({bandwidthDownBits})";
              };
              cpu.interval = 2;
              cpu.format = "cpu load({load}%) use({usage}%)";
              memory.format = "mem {}%";
              # battery
              clock.format = "{:%a %b %d %H:%M}";
            };
        }];
      };
    };
  };
}
