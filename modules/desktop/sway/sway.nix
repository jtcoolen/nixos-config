{ config, options, pkgs, inputs, helpers, lib, ... }:
with lib;
let
  cfg = config.modules.desktop.sway;
  swayfont = {
    names = [ "IBM Plex Sans Bold" ];
    size = 9.0;
  };
  sway_config =
    builtins.foldl' (imports: mod: imports // (import (./. + "/_conf/${mod}")) { pkgs = pkgs; })
      { }
      (helpers.nixFiles ./_conf);
  status_command = "${pkgs.waybar}/bin/waybar";
in
{
  imports = [ _waybar/waybar.nix ];
  options.modules.desktop.sway = {
    enable = mkOption {
      default = false;
      type = with types; bool;
    };
  };

  config = mkIf cfg.enable
    {
      services.pipewire = {
        enable = true;
        socketActivation = true;
      };

      services.upower.enable = true;

      services.gnome.gnome-keyring.enable = true;
      programs.light.enable = true;
      services.gnome.sushi.enable = true;
      programs.seahorse.enable = true;

      services.gvfs.enable = true;
      programs.dconf.enable = true;

      services.redshift = {
        enable = true;
        package = pkgs.redshift-wlr;
        temperature.night = 1500; # Save my eyes
      };

      nix = {
        binaryCachePublicKeys = [
          "nixpkgs-wayland.cachix.org-1:3lwxaILxMRkVhehr5StQprHdEo4IrE8sRho9R9HOLYA="
        ];
        binaryCaches = [ "https://nixpkgs-wayland.cachix.org" ];
        trustedUsers = [ "@wheel" "root" ];
      };

      nixpkgs.overlays = [ inputs.nixpkgs-wayland.overlay ];
      #++ [
      #  (self: super: {
      #    xdg-desktop-portal-wlr = super.xdg-desktop-portal-wlr.overrideAttrs
      #      (oldAttrs: rec {
      #        nativeBuildInputs = oldAttrs.nativeBuildInputs ++ [ pkgs.makeWrapper ];
      #        postInstall = ''
      #          wrapProgram $out/libexec/xdg-desktop-portal-wlr --prefix PATH ":" ${lib.makeBinPath [ pkgs.slurp ]}
      #        '';
      #      });
      #  })
      #];
      programs.qt5ct.enable = true;

      home-manager.users.jco = { pkgs, ... }: {
        home.sessionVariables = {
          MOZ_ENABLE_WAYLAND = "1";
          QT_QPA_PLATFORM = "wayland";
          XDG_SESSION_TYPE = "wayland";
          XDG_CURRENT_DESKTOP = "sway";
          QT_QPA_PLATFORMTHEME = "qt5ct";
          MOZ_DBUS_REMOTE = "1";
          _JAVA_AWT_WM_NONREPARENTING = "1";
        };

        home.packages = with pkgs; [
          glib
          grim
          mako
          flashfocus
          qt5.qtwayland
          redshift-wlr
          slurp
          swaybg
          swayidle
          swaylock
          waypipe
          waybar
          wf-recorder
          wl-clipboard
          xwayland
          bemenu
        ];

        wayland.windowManager.sway = {
          enable = true;
          #systemdIntegration = true; # beta
          #extraSessionCommands = ''
          #  export DBUS_SESSION_BUS_ADDRESS="unix:path=$XDG_RUNTIME_DIR/bus"
          #
          #   systemctl --user import-environment
          #'';
          wrapperFeatures = {
            base = true;
            gtk = true;
          };
          config = rec {
            bars = [{
              command = status_command;
            }];
            fonts = swayfont;
            focus.followMouse = "always";
            focus.newWindow = "urgent";
          } // sway_config;
        };
      };

      programs.sway.enable = true; # needed for swaylock/pam stuff
      programs.sway.extraPackages = [ ];

      xdg = {
        portal = {
          enable = true;
          extraPortals = with pkgs; [
            xdg-desktop-portal-wlr
            #       xdg-desktop-portal-gtk
          ];
          #      gtkUsePortal = true;
        };
      };
    };
}
