{ config, options, pkgs, lib, inputs, ... }:
with lib;
let cfg = config.modules.desktop.gui;
in
{
  options.modules.desktop.gui = {
    enable = mkOption {
      default = false;
      type = with types; bool;
    };

  };

  config = mkIf cfg.enable {
    services.dbus.packages = with pkgs; [ gnome3.dconf ];


    nix = {
      binaryCachePublicKeys = [
        "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      ];
      binaryCaches = [ "https://nix-community.cachix.org" ];
      trustedUsers = [ "@wheel" "root" ];
    };


    home-manager.users.jco = { pkgs, ... }: {
      home.packages = with pkgs; [
        alacritty
        firefox

        gnome3.nautilus
        gnome3.file-roller
        gnome3.gnome-autoar

        libreoffice-fresh
        gnome3.simple-scan

        imv
        krita
        gimp
      ];

      programs.zathura.enable = true;

      programs.mpv = {
        enable = true;
        config = {
          video-sync = "display-resample";
          hwdec = "vaapi";
          vo = "gpu";
          hwdec-codecs = "all";
          gpu-context = "wayland";
        };
      };

      gtk = {
        enable = true;
        iconTheme = {
          name = "Adwaita";
          package = pkgs.gnome3.adwaita-icon-theme;
        };
        font = { name = "IBM Plex Sans Cond Text 11"; };
        theme = { name = "Adwaita"; };
      };

      qt = {
        enable = true;
        platformTheme = "gtk";
      };
    };
  };
}
