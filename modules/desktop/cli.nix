{ config, options, pkgs, lib, ... }:
with lib;
let cfg = config.modules.desktop.cli;
in
{
  options.modules.desktop.cli = {
    enable = mkOption {
      default = false;
      type = with types; bool;
    };
  };

  config = mkIf cfg.enable {

    security.sudo.extraRules = [
      {
        users = [ "jco" ];
        commands = [
          {
            command = "${pkgs.protonvpn-cli}/bin/protonvpn";
            options = [ "NOPASSWD" ];
          }
        ];
      }
    ];
    home-manager.users.jco = { pkgs, ... }: {
      home.packages = (with pkgs; [
        wget
        curl
        wgetpaste
        zip
        unzip
        p7zip

        htop
        lm_sensors

        pijul

        w3m
        nnn
        exa
        ripgrep
	zoxide
	broot
	hyperfine

        neovim
        tmux

        protonvpn-cli
        youtube-dl

        nixpkgs-fmt
        cachix
        any-nix-shell
        nixpkgs-review
      ]);
      programs.bat = {
        enable = true;
        config = {
          pager = "less -FR";
          theme = "ansi";
        };
      };
      programs.fzf.enable = true;
      services.lorri.enable = true;
    };
  };

}
