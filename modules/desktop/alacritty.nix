{ pkgs, config, inputs, lib, ... }:
with lib;
let
  ts = import ./_vars/termsettings.nix { inherit pkgs; };
  font = ts.fonts.default;
  colors = ts.colors.default;
  cfg = config.modules.desktop.alacritty;
in
{
  options.modules.desktop.alacritty = {
    enable = mkOption {
      default = false;
      type = with types; bool;
    };

  };
  config = mkIf cfg.enable {
    home-manager.users.jco = { pkgs, ... }: {
      programs.alacritty = {
        enable = true;
        settings = {
          env = { TERM = "xterm-256color"; };
          window = {
            padding = {
              x = 10;
              y = 10;
            };
          };
          font = {
            normal.family = "${font.name}";
            size = font.size;
            use_thin_strokes = false;
          };
          #draw_bold_text_with_bright_colors = true;
          colors = rec {
            primary.foreground = colors.foreground;
            primary.background = colors.background;

            primary.dim_foreground = colors.dimForeground;
            primary.bright_foreground = colors.brightForeground;

            normal = {
              black = colors.black;
              red = colors.red;
              green = colors.green;
              yellow = colors.yellow;
              blue = colors.blue;
              magenta = colors.purple;
              cyan = colors.cyan;
              white = colors.white;
            };
            bright = {
              black = colors.brightBlack;
              red = colors.brightRed;
              green = colors.brightGreen;
              yellow = colors.brightYellow;
              blue = colors.brightBlue;
              magenta = colors.brightPurple;
              cyan = colors.brightCyan;
              white = colors.brightWhite;
            };
            dim = {
              black = colors.dimBlack;
              red = colors.dimRed;
              green = colors.dimGreen;
              yellow = colors.dimYellow;
              blue = colors.dimBlue;
              magenta = colors.dimPurple;
              cyan = colors.dimCyan;
              white = colors.dimWhite;
            };
          };
        };
      };
    };
  };
}
