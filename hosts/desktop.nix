{ inputs, config, lib, pkgs, helpers, ... }:

{
  imports = helpers.mapModulesRec (toString ../modules);

  # Use the latest Linux kernel.
  boot.kernelPackages = pkgs.linuxPackages_latest;

  modules = {
    desktop = {
      sway.enable = true;
      cli.enable = true;
      gui.enable = true;
      alacritty.enable = true;
      vscode.enable = true;
    };
    dev = {
      cpp.enable = true;
      qemu.enable = true;
      texlive.enable = true;
      docker.enable = true;
      rust.enable = true;
      go.enable = true;
      python.enable = true;
      ocaml.enable = true; # <3
      postgresql.enable = true;
    };
    hardware = {
      audio.enable = true;
      bluetooth.enable = true;
      printers.enable = true;
      opengl.enable = true;
      powersave.enable = true;
    };
    shell = {
      direnv.enable = true;
      git.enable = true;
      fish.enable = true;
      gnupg.enable = true;
    };
  };

  networking.networkmanager.enable = true;

  services.openssh.enable = true;

  # Periodic garbage collection.
  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 30d";
  };

  documentation.enable = true;
  documentation.nixos.enable = false;

  system.stateVersion = "20.09";

  console = {
    font = "Lat9-w16";
    keyMap = "fr";
  };
  i18n.defaultLocale = "fr_FR.UTF-8";
  time.timeZone = "Europe/Paris";

  location = {
    latitude = 48.8;
    longitude = 2.3;
  };

  users.users.jco = {
    isNormalUser = true;
    shell = pkgs.fish;
    extraGroups = [
      "wheel"
      "networkmanager"
      "audio"
      "video"
      "docker"
      "sway"
      "wireshark"
    ];
  };

  nix.trustedUsers = [ "jco" "root" ];

  security.pam.loginLimits = [
    {
      domain = "*";
      item = "nofile";
      type = "hard";
      value = "4096";
    }
  ];

  fonts = {
    fonts = with pkgs; [ ibm-plex stix-two fira charis-sil fira-code jetbrains-mono ];
    fontconfig = {
      defaultFonts = {
        monospace = [ "IBM Plex Mono Text" ];
        sansSerif = [ "IBM Plex Sans" ];
        serif = [ "IBM Plex Serif" ];
      };
    };
  };

}
