{ config, pkgs, lib, inputs, ... }:

{
  imports = [
    inputs.hardware.nixosModules.lenovo-thinkpad-x230
    ../desktop.nix
  ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  hardware.cpu.intel.updateMicrocode =
    lib.mkDefault config.hardware.enableRedistributableFirmware;

  boot.initrd.kernelModules = [ "i915" ]; # remove?

  #boot = {
  #  kernelModules = [ "tpm-rng" "acpi_call" ];
  #  extraModulePackages = with config.boot.kernelPackages; [ acpi_call ];
  #};

  hardware.opengl.extraPackages = with pkgs; [
    vaapiIntel
    vaapiVdpau
    libvdpau-va-gl
  ];

  services.xserver.deviceSection = lib.mkDefault ''
    Option "TearFree" "true"
  '';

  environment.systemPackages = (with pkgs; [ acpi tpacpi-bat ]);

  services.tlp = {
    enable = true;
    settings = {
      # Operation mode when no power supply can be detected: AC, BAT.
      "TLP_DEFAULT_MODE" = "BAT";
      # Operation mode select: 0=depend on power source, 1=always use TLP_DEFAULT_MODE
      "TLP_PERSISTENT_DEFAULT" = 1;
      # See https://wiki.archlinux.org/index.php/ThinkPad_X230
      "START_CHARGE_THRESH_BAT" = 67;
      "STOP_CHARGE_THRESH_BAT0" = 100;
    };
  };

  hardware.opengl.driSupport32Bit = true;

  boot.initrd.availableKernelModules =
    [ "xhci_pci" "ehci_pci" "ahci" "usb_storage" "sd_mod" "sdhci_pci" ];
  boot.kernelModules = [ "kvm-intel" ]; # "iwlwifi"
  boot.extraModulePackages = [ ];

  nix.maxJobs = lib.mkDefault 4;
  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";

  hardware.enableRedistributableFirmware = true;

  networking.useDHCP = false;
  networking.interfaces.enp0s25.useDHCP = true;
  networking.interfaces.wlp3s0.useDHCP = true;
}
