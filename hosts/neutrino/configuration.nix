{ config, pkgs, lib, inputs, ... }:

{
  imports = [
    inputs.hardware.nixosModules.lenovo-thinkpad-x230
    ../desktop.nix
  ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp0s25.useDHCP = true;
  networking.interfaces.wlp3s0.useDHCP = true;

  environment.systemPackages = with pkgs; [ acpi ];

  hardware.cpu.intel.updateMicrocode = true;

}
