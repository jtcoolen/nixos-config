{
  description = "nixos-config";

  inputs = {
    nixpkgs-unstable = { url = "github:nixos/nixpkgs/nixos-unstable"; };
    nixpkgs-stable = { url = "github:nixos/nixpkgs/nixos-20.09"; };
    hardware = { url = "github:nixos/nixos-hardware"; };
    home-manager = { url = "github:nix-community/home-manager"; };
    nixpkgs-wayland = { url = "github:colemickens/nixpkgs-wayland"; };
    nixpkgs-wayland.inputs.nixpkgs.follows = "nixpkgs-unstable";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    rust-overlay.url = "github:oxalica/rust-overlay";
  };

  outputs = inputs:
    let
      helpers = (import ./helpers.nix { nixpkgs = inputs.nixpkgs-unstable; });
      mkHost = name:
        inputs.nixpkgs-unstable.lib.nixosSystem rec {
          system = "x86_64-linux";
          modules = with inputs.nixpkgs-unstable;
            [
              {
                nixpkgs.config = { allowUnfree = true; };
                networking.hostName = name;
              }
              inputs.home-manager.nixosModules.home-manager
              {
                home-manager.useGlobalPkgs = true;
                home-manager.useUserPackages = true;
              }
            ] ++
            (helpers.mapModulesRec (./. + "/hosts/${name}")) ++ [{
              nixpkgs.overlays =
                [
                  (
                    let
                      overlay-stable = final: prev: {
                        stable = inputs.nixpkgs-stable.legacyPackages.${system};
                      };
                    in
                    overlay-stable
                  )
                  (import ./pkgs/pkgs.nix)
                ];
            }];
          specialArgs = {
            inherit inputs;
            inherit helpers;
          };
        };
    in
    {
      nixosConfigurations =
        builtins.mapAttrs (name: _: mkHost name) (builtins.readDir ./hosts);
    };
}
