{ nixpkgs }:
with nixpkgs.lib;
let
  inherit (builtins) attrValues readDir pathExists concatLists mapAttrs;
  inherit (nixpkgs.lib) id mapAttrsToList filterAttrs hasPrefix hasSuffix nameValuePair removeSuffix;
  inherit (self.attrs) mapFilterAttrs;
in
rec {

  # List nix files from a specific directory
  nixFiles = dir:
    attrValues (mapAttrs (name: _: name)
      (filterAttrs (name: _: (hasSuffix ".nix" name)) (readDir dir)));

  # The following functions are taken from https://github.com/hlissner/dotfiles/blob/master/lib/modules.nix
  mapFilterAttrs = pred: f: attrs: filterAttrs pred (mapAttrs' f attrs);

  mapModules = dir:
    mapFilterAttrs
      (n: v:
        v != null &&
        !(hasPrefix "_" n))
      (n: v:
        let path = "${toString dir}/${n}"; in
        if v == "directory" && pathExists "${path}/default.nix"
        then nameValuePair "" null
        else if v == "regular" &&
          n != "default.nix" &&
          hasSuffix ".nix" n
        then nameValuePair (removeSuffix ".nix" n) (import path)
        else nameValuePair "" null)
      (readDir dir);

  mapModulesRec = dir:
    let
      dirs =
        mapAttrsToList
          (k: _: "${dir}/${k}")
          (filterAttrs
            (n: v: v == "directory" && !(hasPrefix "_" n))
            (readDir dir));
      files = attrValues (mapModules dir);
      paths = files ++ concatLists (map (d: mapModulesRec d) dirs);
    in
    paths;
}
