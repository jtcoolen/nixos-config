#!/run/current-system/sw/bin/bash

read -r -p "Input target disk (eg: /dev/sda)" DISK

# format and create partitions
parted /dev/sda -- mklabel gpt
parted /dev/sda -- mkpart primary 512MiB 100%
parted /dev/sda -- mkpart ESP fat32 1MiB 512MiB
parted /dev/sda -- set 2 esp on

cryptsetup luksFormat "${DISK}1"
cryptsetup luksOpen "${DISK}1" cryptroot
mkfs.ext4 -L nixos /dev/mapper/cryptroot
mount /dev/disk/by-label/nixos /mnt

mkfs.fat -F 32 -n boot "${DISK}2"
mkdir -p /mnt/boot
mount /dev/disk/by-label/boot /mnt/boot

nixos-generate-config --root /mnt
